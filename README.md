# Group Anarchy Enforcer

Telegram group chats are authoritarian by default; each group has a particular
set of members, so-called "admins", that enjoys unjust power over the rest of
members. This bot enforces anarchy in your group by making sure (up to the
technical limitation) all members are automacitally admins and having the full
permissions applicable.

The bot is currently written in *PHP* and does not depend on external packages.
It simply uses the Telegram Bot HTTP API, which is limited for a task like this.
Thus, migration to MTProto is planned.


### Sample instance

I host a sample instance for [@GroupAnarchy_bot](https://t.me/GroupAnarchy_bot),
the instance fully complies to this source tree.

*Consider running your own instance instead of using the sample one.*


## Deploying

Since the codebase is apparently *Heroku*-ready, I'll only provide instructions
for deploying on Heroku. You have to modify the source code a little bit to be
able to use it outside the Heroku platform.

1. Register a new bot on Telegram and a new app on Heroku
1. Clone this repo and add your Heroku app's git remote
1. Set `BOT_TOKEN` config var to your Telegram bot token
1. Set `USER_NAME` config var to your Telegram bot username
1. Set `APP_NAME` config var to your Heroku app name
1. Push the code to Heroku

Don't forget to set the webhook by calling:
```sh
curl "https://$APP_NAME.herokuapp.com.com/set_$TOKEN.php"
```

Make sure to enable the bot to join groups and add the following command:

```txt
enforce - Enforce that you (or another member) is an admin in this group
```


## License

Licensed under the [MIT (Expat)](LICENSE) license.

> Copyright (C) 2020, 2021 Karam Assany (karam.assany@disroot.org)
