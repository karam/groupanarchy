<?php
	$un = getenv('USER_NAME');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<style>
			html {height: 100%; width: 100%; display: table;}
			body {
				padding: 2em;
				font-family: sans-serif;
				font-weight: bold;
				display: table-cell;
				text-align: center;
				vertical-align: middle;
				background-color: #a00505;
				color: white;
			}
			a:any-link{color: white;}
			header {font-size: 3em; padding: 1em;}
			main {padding: 3em;}
			footer {padding: 1.5em; font-size: 0.9em; line-height: 1.7em;}
		</style>
		<title>Group Anarchy Bot Instance</title>
	</head>
	<body>
		<header>
			Group Anarchy Bot Instance
		</header>
		<main>
			This is an instance of <i>Group Anarchy Bot</i> for Telegram. You can use
			it (via
			<a href="https://t.me/<?php echo($un);?>">
				@<?php echo($un);?>
			</a>)
			or <a href="/repo">host your own instance</a>.
		</main>
		<footer rel="license">
		Copyright &copy; 2020, 2021 Karam Assany
		<a href="mailto:karam.assany@disroot.org">
			&lt;karam.assany@disroot.org&gt;
				</a><br />
		<a href="/repo">Group Anarchy Bot</a> is licensed under the MIT (Expat)
				license
		</footer>
	</body>
</html>
<!-- vi: set ts=2 sw=2: -->
