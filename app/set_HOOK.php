<?php

$token = getenv('BOT_TOKEN');
$app_name = getenv('APP_NAME');

$url = "https://api.telegram.org:443/bot$token/setWebhook"
       ."?url=https://$app_name.herokuapp.com/$token.php";

header('Content-Type: application/json');
echo( file_get_contents($url) );
